from django.views.generic import TemplateView
from django.shortcuts import get_object_or_404
from .models import Book, Author, AwardPage


class HomeView(TemplateView):
    template_name = 'content/home.html'

    def get_context_data(self, **kwargs):
        query_params = self.request.GET
        current_year_winners = AwardPage.objects.filter(year=2016)
        context = super(HomeView, self).get_context_data(**kwargs)
        context['current_year_winners'] = current_year_winners
        return context


class MissionPageView(TemplateView):
    template_name = 'content/mission.html'


class WinnerPageView(TemplateView):
    template_name = 'content/fiction_winner.html'

    def get_context_data(self, **kwargs):
        query_params = self.request.GET
        query_dict = {}
        if 'year' in query_params:
            query_dict['year'] = query_params.get('year')
        if 'author_name' in query_params:
            query_dict['book__author__name'] = query_params.get('author_name')
        if 'book_title' in query_params:
            query_dict['book__title'] = query_params.get('book_title')
        if 'category' in query_params:
            query_dict['category__name'] = query_params.get('category')
        context = super(WinnerPageView, self).get_context_data(**kwargs)
        context['page'] = get_object_or_404(AwardPage, **query_dict)
        return context


class PastAwardsPageView(TemplateView):
    template_name = 'content/past_awards.html'

    def get_context_data(self, **kwargs):
        context = super(PastAwardsPageView, self).get_context_data(**kwargs)
        years = AwardPage.objects.all().order_by('year').values_list('year', flat=True).distinct()
        awards = AwardPage.objects.all()
        context['awards'] = awards
        context['years'] = years
        return context
