from django.contrib import admin
from .models import *


admin.site.register(Page)
admin.site.register(Author)
admin.site.register(Book)
admin.site.register(AwardCategory)
admin.site.register(AwardPage)
admin.site.register(AdditionalAwardPageContent)
