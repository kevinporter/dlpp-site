from django.db import models


class Page(models.Model):
    title = models.CharField(max_length=255)


class Author(models.Model):
    name = models.CharField(max_length=255)
    quote = models.TextField()
    bio = models.TextField()
    image = models.ImageField()

    def __str__(self):
        return self.name


class Book(models.Model):
    title = models.CharField(max_length=255)
    summary = models.TextField()
    summary_citation = models.TextField()
    author = models.ForeignKey(Author)
    cover = models.ImageField()

    def __str__(self):
        return self.title


class AwardCategory(models.Model):
    name = models.CharField(unique=True, max_length=255)

    def __str__(self):
        return self.name


class AwardPage(models.Model):
    category = models.ForeignKey(AwardCategory)
    year = models.PositiveIntegerField()
    book = models.ForeignKey(Book)

    def __str__(self):
        return '%s - %s (%s %s)' % (self.book.title, 
            self.book.author.name, str(self.year), self.category.name)


class AdditionalAwardPageContent(models.Model):
    name = models.CharField(max_length=255)
    content = models.TextField()

    def __str__(self):
        return self.name
