function setJumbotronCss() {
$('#dlpp-jumbotron').find('p.visible-md.visible-lg').css('margin-top', function () {
		$(this).css('margin-top', 0);
		return $('#dlpp-jumbotron').outerHeight() - $(this).outerHeight();
	});
}

$(document).ready(function () {
	setJumbotronCss();
	$(window).resize(function () {
		setJumbotronCss();
	});
});
